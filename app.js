Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.panel.*', 'Ext.layout.container.Border']);
Ext.onReady(function() {
	Ext.define('Contacto', {
		extend : 'Ext.data.Model',
		fields : ['id', 'nome', 'telefone', 'email']
	});
	var store = Ext.create('Ext.data.Store', {
		model : 'Contacto',
		autoSync : true, // atualiza na BD sempre que sofrer alterações...
		autoLoad : true,
		pageSize : 35,
		autoLoad : {
			start : 0,
			limit : 35
		},
		proxy : {
			type : 'ajax',
			api : {
				create : 'php/criaContacto.php',
				read : 'php/listaContactos.php',
				update : 'php/atualizaContacto.php',
				destroy : 'php/removeContacto.php',
			},
			reader : {
				type : 'json',
				root : 'contactos',
				successProperty : 'success'
			},
			writer : {
				type : 'json',
				writeAllFields : false,
				encode : true,
				root : 'contactos'
			}
		}
	});
	var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
		listeners : {
			cancelEdit : function(rowEditing, context) {
				// Canceling editing of a locally added, unsaved record: remove it
				if (context.record.phantom) {
					store.remove(context.record);
				}
			}
		}
	});
	var grid = Ext.create('Ext.grid.Panel', {
		store : store,
		title : 'Contactos',
		columns : [{
			text : "Id",
			width : 40,
			dataIndex : 'id'
		}, {
			text : "Nome",
			flex : 1,
			dataIndex : 'nome',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Telefone",
			width : 115,
			dataIndex : 'telefone',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Email",
			width : 120,
			dataIndex : 'email',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}],
		viewConfig : {
			forceFit : true
		},
		region : 'center',
		tbar : [{
			text : 'Novo contacto',
			handler : function() {
				rowEditing.cancelEdit();
				// Create a model instance
				var r = Ext.create('Contacto', {
					nome : 'Fulano',
					telefone : '987654321',
					email : 'fulano@sapo.pt'
				});
				store.insert(0, r);
				rowEditing.startEdit(0, 0);
			}
		}, {
			itemId : 'apagaContacto',
			text : 'Elimina contacto',
			handler : function() {
				var sm = grid.getSelectionModel();
				rowEditing.cancelEdit();
				store.remove(sm.getSelection());
				if (store.getCount() > 0) {
					sm.select(0);
				}
			},
			disabled : true
		}],
		plugins : [rowEditing],
		listeners : {
			'selectionchange' : function(view, records) {
				grid.down('#apagaContacto').setDisabled(!records.length);
			}
		}
	});
	Ext.widget('tabpanel', {
		renderTo : 'tabela',
		width : 600,
		height : 400,
		activeTab : 0,
		defaults : {
			bodyPadding : 20
		},
		items : [{
			html : "Lista dos meus contactos.",
			title : 'Introdu��o',
			closable : true
		}, {
			title : 'Contactos',
			items : [grid]
		},
		{
                title: 'Normal Tab',
                html: "My content was added during construction."
            },{
                title: 'Ajax Tab 1',
                loader: {
                    url: 'ajax1.htm',
                    contentType: 'html',
                    loadMask: true
                },
                listeners: {
                    activate: function(tab) {
                        tab.loader.load();
                    }
                }
            },{
                title: 'Ajax Tab 2',
                loader: {
                    url: 'ajax2.htm',
                    contentType: 'html',
                    autoLoad: true,
                    params: 'foo=123&bar=abc'
                }
            },{
                title: 'Event Tab',
                listeners: {
                    activate: function(tab){
                        setTimeout(function() {
                            alert(tab.title + ' was activated.');
                        }, 1);
                    }
                },
                html: "I am tab 4's content. I also have an event listener attached."
            },{
                title: 'Disabled Tab',
                disabled: true,
                html: "Can't see me cause I'm disabled"
            }
		]
	});
	/*Ext.create('Ext.panel.Panel', {
		renderTo : 'tabela',
		width : '600px',
		height : '400px',
		frame : true,
		layout : 'border',
		items : [grid]
	});
	*/
});
